import getpass
import os
import sys
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
    
    if not os.path.exists('config'):
        os.makedirs('config')
    print("Setting up fediverse parameters...")
    print("\n")
    fediverse_db = input("fediverse db name: ")
    fediverse_db_user = input("fediverse db user: ")
    mastodon_db = input("Mastodon db name: ")
    mastodon_db_user = input("Mastodon db user: ")
  
    with open(file_path, "w") as text_file:

        print("fediverse_db: {}".format(fediverse_db), file=text_file)
        print("fediverse_db_user: {}".format(fediverse_db_user), file=text_file)
        print("mastodon_db: {}".format(mastodon_db), file=text_file)
        print("mastodon_db_user: {}".format(mastodon_db_user), file=text_file)

def create_table(db, db_user, table, sql):

    conn = None

    try:

        conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
        cur = conn.cursor()

        print("Creating table.. "+table)
        # Create the table in PostgreSQL database
        cur.execute(sql)

        conn.commit()

        print("Table "+table+" created!")
        print("\n")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

###############################################################################
# main

if __name__ == '__main__':

    # Load configuration from config file
    config_filepath = "config/db_config.txt"
    fediverse_db = get_parameter("fediverse_db", config_filepath)
    fediverse_db_user = get_parameter("fediverse_db_user", config_filepath)
    mastodon_db = get_parameter("mastodon_db", config_filepath)
    mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)

    ############################################################
    # create database
    ############################################################

    conn = None

    try:

        conn = psycopg2.connect(dbname='postgres',
            user=fediverse_db_user, host='',
            password='')

        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

        cur = conn.cursor()

        print("Creating database " + fediverse_db + ". Please wait...")

        cur.execute(sql.SQL("CREATE DATABASE {}").format(
              sql.Identifier(fediverse_db))
            )
        print("Database " + fediverse_db + " created!")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    #############################################################################################

    try:

        conn = None
        conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)
        # Load configuration from config file
        os.remove("db_config.txt")
        print("Exiting. Run db-setup again with right parameters")
        sys.exit(0)

    if conn is not None:

        print("\n")
        print("fediverse parameters saved to db-config.txt!")
        print("\n")

    ############################################################
    # Create needed tables
    ############################################################

    print("Creating table...")

    ########################################

    db = fediverse_db
    db_user = fediverse_db_user
    table = "world"
    sql = "create table "+table+" (server varchar(200) PRIMARY KEY, federated_with varchar(200), updated_at timestamptz, saved_at timestamptz, checked boolean)"
    create_table(db, db_user, table, sql)

    table = "fediverse"
    sql = "create table "+table+" (server varchar(200) PRIMARY KEY, users INT, updated_at timestamptz, software varchar(50), alive boolean, users_api varchar(50), version varchar(100), first_checked_at timestamptz, last_checked_at timestamptz, downs int)"
    create_table(db, db_user, table, sql)

    table = "blocked"
    sql = "create table "+table+" (server varchar(200) PRIMARY KEY, users INT, updated_at timestamptz, software varchar(50), alive boolean, users_api varchar(50), version varchar(100), first_checked_at timestamptz, last_checked_at timestamptz, downs int)"
    create_table(db, db_user, table, sql)

    table = "totals"
    sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, total_servers INT, total_users INT)"
    create_table(db, db_user, table, sql)

    table = "evo"
    sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, servers INT, users INT)"
    create_table(db, db_user, table, sql)

    table = "execution_time"
    sql = "create table "+table+" (program varchar(20) PRIMARY KEY, start timestamptz, finish timestamptz)"
    create_table(db, db_user, table, sql)

    #####################################

    print("Done!")
    print("Now you can run setup.py!")
    print("\n")
