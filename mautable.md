|   software  |                API                | active users |              values             |
|:-----------:|:---------------------------------:|:------------:|:-------------------------------:|
| Akkoma      | /nodeinfo/2.0.json                | No           |                                 |
| bonfire     | /.well-known/nodeinfo/2.0         | No           |                                 |
| bookwyrm    | /nodeinfo/2.0                     | Yes          | "activeMonth", "activeHalfyear" |
| brutalinks  | /nodeinfo                         | No           |                                 |
| calckey     | /nodeinfo/2.0                     | Yes          | "activeHalfyear", "activeMonth" |
| diaspora    | /nodeinfo/1.0                     | Yes          | "activeHalfyear", "activeMonth" |
| ecko        | /nodeinfo/2.0                     | Yes          | "activeMonth", "activeHalfyear" |
| friendica   | /nodeinfo/1.0                     | Yes          | "activeMonth", "activeHalfyear" |
| funkwhale   | /api/v1/instance/nodeinfo/2.0     | Yes          | "activeHalfyear","activeMonth"  |
| gancio      | /.well-known/nodeinfo/2.0         | No           |                                 |
| gitea       | /api/v1/nodeinfo                  | Yes          | "activeHalfyear", "activeMonth" |
| gnusocial   | /api/nodeinfo/2.0.json            | Yes          | "activeHalfyear", "activeMonth" |
| hometown    | /nodeinfo/2.0                     | Yes          | "activeMonth","activeHalfyear"  |
| hubzilla    | /nodeinfo/2.0                     | Yes          | "activeHalfyear","activeMonth"  |
| lemmy       | /nodeinfo/2.0.json                | Yes          | "activeHalfyear","activeMonth"  |
| mastodon    | /nodeinfo/2.0                     | Yes          | "activeMonth","activeHalfyear"  |
| misskey     | /nodeinfo/2.0                     | Yes          | "activeHalfyear","activeMonth"  |
| mobilizon   | /.well-known/nodeinfo/2.0         | No           |                                 |
| owncast     | /nodeinfo/2.0                     | Yes          | "activeMonth","activeHalfyear"  |
| peertube    | /nodeinfo/2.0.json                | Yes          | "activeMonth","activeHalfyear"  |
| pixelfed    | /api/nodeinfo/2.0.json            | Yes          | "activeHalfyear","activeMonth"  |
| pleroma     | /nodeinfo/2.0.json                | Yes (develop)|                                 |
| plume       | /nodeinfo/2.0                     | No           |                                 |
| smithereen  | /activitypub/nodeinfo/2.1         | Yes          | "activeMonth","activeHalfyear"  |
| wordpress   | /wp-json/activitypub/1.0/nodeinfo | No           |                                 |
| writefreely | /api/nodeinfo                     | Partial      | "activeHalfyear"                |
| zap         | /nodeinfo/2.0                     | Yes          | "activeHalfyear","activeMonth"  |
