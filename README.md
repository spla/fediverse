# Fediverse Stats
This code gets all peers from running Mastodon, Pleroma and Lemmy host servers and then all peers from host server's peers. Goal is to collect maximum number
of alive fediverse's servers and then query their API to obtain their registered users (if their API provide such information).
At the end it post the results to host server bot account.

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon or Pleroma running server.

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed libraries.

2. Run `python db-setup.py` to setup and create new Postgresql database and needed tables in it.

3. Run `python setup.py` to get your bot's access token of your Mastodon or Pleroma server existing account. It will be saved to 'secrets/secrets.txt' for further use.

4. Run `python getpeers.py` to get all peers from your host and the whole world of fediverse's servers (or almost the whole world).

5. Run `python fetchservers.py` to add servers to alive servers database.

6. Run `python fediverse.py` to query world alive servers API. It gets data from server's nodeinfo.

7. Run `python uptime_setup.py` to get your Uptime bot's access token of your Mastodon or Pleroma server existing account. It will be saved to 'secrets/uptime_secrets.txt' for further use.

8. Use your favourite scheduling method to set `python fediverse.py` to run twice daily, `python fetchservers.py` one time daily, `python getworld.py` to run monthly and `python uptime.py` (choose your desired frequency) if you want to publish best fediverse's servers uptime.   

18.2.2021 - New feature! Added [Lemmy project](https://join.lemmy.ml)  
12.5.2021 - New feature! Added Wordpress support. The code can now detect Wordpress instances with ActivityPub enabled plugin.  
12.5.2021 - New feature! New shinny creation of servers and users graphs.  
21.8.2021 - New feature! Added Best Fediverse's servers Uptime publishing bot.  
22.10.2021 - New feature! Added [Funkwhale](https://funkwhale.audio) support.  
26.10.2021 - New feature! Added [Socialhome](https://socialhome.network) support.  
2.3.2022 - Improved server nodeinfo detection
